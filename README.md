# bantam-tabs

## Purpose

The `bantam-tabs` module implements fading tab functionality for BantamJS. A list of links, styled as tabs, are linked to a series of divs by ID. When the relevant tab is clicked, the tab pane that is currently being shown fades out and is replaced by the new one.

## Installation

The Javascript script can be downloaded [here](files/1.0/bantam-tabs-1.0.min.js) and the CSS can be downloaded [here](files/1.0/bantam-tabs-1.0.min.css). These files can be linked directly into your app (they require [BantamJS](https://canderegg.gitlab.io/bantam-js/) as a prerequisite). Alternatively, `bantam-tabs` is a module available through the [Bantam Coop](https://canderegg.gitlab.io/bantam-coop/) module manager.

To install the module through Bantam Coop, use the following command:

```bash
$ python coop.py add bantam-tabs +1.0
```

## Usage

The tabs themselves are contained within an unordered list with the `tab-list` class. Each list element should contain a link with a hash ID reference to one of the tab panes. When the page is loaded, the first tab will be marked as active.

The tab panes themselves should be divs, each with the `tab-pane` class, and IDs corresponding to those within the tab links. All the related tab panes should be included in div element with the `tab-conent` class. This parent-level div is used to group related tab panes in the event that you want several sets of tabs on a single page. The tab that is currently being displayed will have the `tab-in` CSS class.

The code for an example set of tabs might look like this:

```html
<ul class="tab-list">
  <li><a href="#tab1">Tab 1</a></li>
  <li><a href="#tab2">Tab 2</a></li>
  <li><a href="#tab3">Tab 3</a></li>
</ul>

<div class="tab-content">
  <div id="tab1" class="tab-pane tab-in">
    <h3>Tab 1</h3>
    <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Praesent cursus 
       orci sapien, ac porttitor odio cursus sit amet. Nunc quis facilisis lacus. 
       Vestibulum ligula diam, tempus nec neque eget, fringilla tempus mauris.</p>
  </div>

  <div id="tab2" class="tab-pane">
    <h3>Tab 2</h3>
    <p>Nulla laoreet massa enim, sed feugiat nunc tincidunt at. Proin commodo, 
       ipsum nec mattis luctus, leo turpis vestibulum massa, at vulputate quam 
       quam ac tortor. Nullam purus tellus, consequat eu tellus quis, hendrerit 
       lobortis ipsum.</p>
  </div>

  <div id="tab3" class="tab-pane">
    <h3>Tab 3</h3>
    <p>Vestibulum vel ultricies sapien. Mauris molestie sit amet quam eu dictum. 
       Nunc ac odio nec lorem facilisis ultrices. Maecenas rutrum, neque in 
       pretium placerat, leo lorem venenatis enim, id maximus est mi congue purus.</p>
  </div>
</div>
```

## License

This module is distributed under the [MIT License](LICENSE).
