(function() {
  $bt.tabs = {};

  // Create the tab-switching function
  $bt.tabs.showtab = function() {
    // Loop over the tabs in this tab list
    let tabList = this.parentElement.parentElement;
    let tabs = tabList.get('li');
    for (let i=0; i<tabs.length; i++) {
      // Remove the tab activation
      tabs[i].erase('tab-in');
      // Hide the tab pane
      let pane = $bt.get(tabs[i].get('a')[0].hash);
      pane.erase('tab-in');
    }
    // Set this tab as active
    this.parentElement.add('tab-in');
    let pane = $bt.get(this.hash);
    setTimeout(function() { pane.add('tab-in'); }, 150);
    return false;
  };

  // Find all the tab lists to look at
  let tabComponents = $bt.get('.tab-list');
  for (let i=0; i<tabComponents.length; i++) {
    // Set the first tab as active
    let tabs = tabComponents[i].get('li');
    tabs[0].add('tab-in');
    for (let j=0; j<tabs.length; j++) {
      tabs[j].get('a')[0].onclick = $bt.tabs.showtab;
    }
  }
  // Size all the tab-content elements
  let tabContents = $bt.get('.tab-content');
  for (let i=0; i<tabContents.length; i++) {
    let panes = tabContents[i].get('.tab-pane');
    let max = 0;
    for (let j=0; j<panes.length; j++) {
      max = Math.max(max, panes[j].clientHeight);
    }
    tabContents[i].style['height'] = max + 'px';
  }
})();
